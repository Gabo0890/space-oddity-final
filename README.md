@@ -1,14 +1,21 @@
@@ -1,45 +1,37 @@
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**
## Space Oddity in VR

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.


*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*


Dies ist die Reise von David Bowies Major Tom durch Raum und Zeit in virtueller Realität.
Zu dem Original Song "Space Oddity" von David Bowie begibt sich der Nutzer in die Rolle eines Astronauten (je nach eigener interpretation Major Tom) und erkundet entfernte Weiten im Weltraum.
Während der Nutzer die Story von Major Tom durch den Song hört, hat er gleichzeitig die Möglichkeit das Weltall selbstständig zu erleben.
Der Nutzer hat dabei die Möglichkeiten sich frei umzusehen und durch Portale in andere Welten zu wechseln.

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.
Es wird ein interaktives Storytelling erzeugt. Der Nutzer hört das Schicksal von Major Tom, wie er sich langsam im Weltraum verliert, ohne einen Einfluss darauf zu haben. Doch gleichzeitg ist er selber in dieser Atmosphäre und hat einen Einfluss auf seine eigene Bewegung durch diese Situation.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
@@ -16,10 +23,14 @@ You’ll start by editing this README file to learn how to edit a file in Bitbuc
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.




Ziel ist es, dass sich der Nutzer selbst im Weltraum bewegt und sich immer weiter von seiner Ursprungsposition wegbewegt ohne zurück kommen zu können, wie Major Tom. Durch die eigene Erfahrung die er macht und durch die erzeugte Atmosphäre, erlebt er den Song aktiv.
Auf seiner Reise sieht er verschiedene Objekte, Wesen und Welten. Manche interagieren mit dem Nutzer, indem sie durch Textkacheln Erläuterungen oder Zitate bereithalten. Andere bleiben dem Nutzer unerschlossen.
Die Illusion des Astronauten bekommt der Nutzer durch ein integriertes Head-Up Display, welches stehts vor seiner Sicht liegt. Dort eingebaut ist eine Sauerstoffleiste, welche mit der Tonspurt des Songs verbunden ist. Der Nutzer sieht seinen eigenen Sauerstoff schwinden, weiß aber nicht, was am Ende geschehen wird. Dies verstärkt den Effekt des verloren Gehens in Zeit und Raum, wie es Major Tom erlebt.

---

## Create a file
## Stand des Projektes am 27.07.2018

Next, you’ll add a new file to this repository.

@@ -30,16 +41,35 @@ Next, you’ll add a new file to this repository.
Discard Hunk
Stage Hunk
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.




1. Auswahl Menü um die Anwendung in der VR Brille aktiv zu starten
2. Die Tonspur beginnt mit einem Start in den Weltraum, um den Nutzer in die Situation hineinzuversetzen. Der Raketenstart in der Tonspur geht in die Atemgeräusche einer Sauerstoffflasche über, in welches Space Oddity von David Bowie eingespielt wird.
3. Es gibt vier Scenes. Die erste ist frei im Weltamm. Die zweite eine Landschaft, welche dem Mars ähnelt. Die dritte und vierte sind unbekannte Phantasiewelten.
4. Der Wechsel über die Scenes erfolgt über Portale im Weltraum, welche durch einen CLick von Nutzer angesteuert werden. Die Portale sind über eine Kamera in der nächsten Scene so konzipiert, dass der Nutzer durch das Portal in die nächste Welt blicken kann, ohne durch das Portal zu schreiten. So sieht er was ihn erwartet und er kann selbst entscheiden, ob er dorthin wechselt oder nicht.
5. Der Nutzer schaut in der gesamten Anwendung durch ein Head Up Display. Dieses enthält eine Sauerstoffleiste, welche mit der Tonspur abläuft. **Problem:** Die Sauerstoffleiste läd bei jedem Szenenwechsel neu. Dies soll noch repariert werden.

---

## Clone a repository
## Verwendete Software für die VR Anwendung

1. Unity Version 2017.3
+
2. Google VR for Unity SDK (mobile Anwendung für Google Cardboard)

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).
## Modellierung

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
\ No newline at end of file



1. Blender 2.7
2. Crazy Bump (Texture Maps für Planeten)
3. Adobe Photoshop

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
\ No newline at end of file
File contents are unchanged.
47 file changes in working directory
View changes
commit:9092ee
README.md Space Oddity
48 file changes onziggyfinal
Name

Path

Tree
Unstaged Files (8)
Stage all changes
Expand All
UnityProjekt
6
.DS_Store
README.md
Staged Files (40)
Unstage all changes
Expand All
UnityProjekt
28
6
6
Commit Message

Amend

Summary

Description
